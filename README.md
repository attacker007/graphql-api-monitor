# GraphQL API Monitor

It shouldn't come as a surprise that this tool monitors GraphQL APIs. :) Feed it URLs that return GraphQL schema files or APIs that support introspection and each time you run the tool it will compare the version it downloads to the previous one using a `git` repository. If there are diffs and if a webhook has been configured, it will send the diff to the webhook. I recommend setting it up on a cron job to get daily updates!

The tool only supports Discord webhooks at the moment because this is what I use but I definitely will accept MRs that add support for Slack and other webhooks.

See https://twitter.com/dee__see/status/1286820045369221120 for a screenshot of what the notifications look like.

## Installation

```shell
git clone https://gitlab.com/dee-see/graphql-api-monitor.git
cd graphql-api-monitor
npm install
npm run build
```

Requires node.js version 12+ and assumes that `git` is installed on the machine.

## Usage

Create a config file (see `config_example.json`) and then run `node dist/index.js --config /path/to/config.json`

### Configuration

```json
{
  "dataDirectory": "/path/to/where/data/is/saved", # Optional, defaults to current directory
  "webhook": "https://discord.com/api/webhooks/ABCD", # Optional
  "targets": [
    {
      "name": "HackerOne",
      "url": "https://hackerone.com/graphql",
      "verb": "POST", # Optional, default value is POST, possible values are POST or GET
      "type": "Introspection", # Optional, default value is Introspection, possible values are Introspection or Schema
      "headers": { # Optional
        "X-Random-Header": "123",
        "Authorization": "Basic ___ENV-SECRET_TOKEN___"
      }
    }
  ]
}
```

### Authentication

You can user the `headers` settings as in the example to configure `Authentication` headers. If you're uncomfortable with hardcoding credentials in a config file (as you should be!), you can instead put `___ENV-VARIABLE_NAME_HERE___` anywhere in the configuration values and it will be replaced at runtime by the value of the `VARIABLE_NAME_HERE` environment variable.

## Why node.js

Because the [`graphql-js`](https://github.com/graphql/graphql-js/) package is awesome and made things so much more simple.
